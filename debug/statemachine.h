﻿//
// Statemachine code from reading SCXML file 'statemachine.scxml'
// Created by: The Qt SCXML Compiler version 1 (Qt 5.7.0)
// WARNING! All changes made in this file will be lost!
//

#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <QScxmlStateMachine>
#include <QString>
#include <QByteArray>

class CalculatorStateMachine: public QScxmlStateMachine
{
public:
    /* qmake ignore Q_OBJECT */
    Q_OBJECT
    Q_PROPERTY(bool wrapper READ wrapper NOTIFY wrapperChanged)
    Q_PROPERTY(bool on READ on NOTIFY onChanged)
    Q_PROPERTY(bool ready READ ready NOTIFY readyChanged)
    Q_PROPERTY(bool begin READ begin NOTIFY beginChanged)
    Q_PROPERTY(bool result READ result NOTIFY resultChanged)
    Q_PROPERTY(bool negated1 READ negated1 NOTIFY negated1Changed)
    Q_PROPERTY(bool operand1 READ operand1 NOTIFY operand1Changed)
    Q_PROPERTY(bool zero1 READ zero1 NOTIFY zero1Changed)
    Q_PROPERTY(bool int1 READ int1 NOTIFY int1Changed)
    Q_PROPERTY(bool frac1 READ frac1 NOTIFY frac1Changed)
    Q_PROPERTY(bool opEntered READ opEntered NOTIFY opEnteredChanged)
    Q_PROPERTY(bool negated2 READ negated2 NOTIFY negated2Changed)
    Q_PROPERTY(bool operand2 READ operand2 NOTIFY operand2Changed)
    Q_PROPERTY(bool zero2 READ zero2 NOTIFY zero2Changed)
    Q_PROPERTY(bool int2 READ int2 NOTIFY int2Changed)
    Q_PROPERTY(bool frac2 READ frac2 NOTIFY frac2Changed)

public:
    CalculatorStateMachine(QObject *parent = 0);
    ~CalculatorStateMachine();

protected:
    void setService(const QString &id, QScxmlInvokableService *service) Q_DECL_OVERRIDE Q_DECL_FINAL;

private:
    struct Data;
    friend struct Data;
    struct Data *data;
};

Q_DECLARE_METATYPE(::CalculatorStateMachine*);

#endif // STATEMACHINE_H
